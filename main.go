package main

import (
	"log"

	"github.com/gofiber/fiber"
	"github.com/joho/godotenv"
	"github.com/ranggaadi/golang-mongo-api/controllers"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	app := fiber.New()

	app.Get("/", controllers.Landing())

	api := app.Group("/api")

	v1 := api.Group("/v1")

	//Article
	v1.Get("/articles", controllers.GetAllArticle())    //
	v1.Get("/articles/:slug", controllers.GetArticle()) //
	v1.Get("/articles/t/:tag", controllers.GetArticleByTag())

	//Auth
	v1.Post("/users/signup", controllers.SignUp())                        //
	v1.Get("/users/logout", controllers.Logout())                         //
	v1.Post("/users/login", controllers.Login())                          //
	v1.Patch("/users/reset-password/:token", controllers.ResetPassword()) //
	v1.Post("/users/forgot-password", controllers.ForgotPassword())       //

	v1.Use(controllers.Protect()) //must login

	v1.Post("/articles", controllers.CreateArticle())                                     //
	v1.Patch("/articles/:slug", controllers.IsAuthorized(), controllers.UpdateArticle())  //
	v1.Delete("/articles/:slug", controllers.IsAuthorized(), controllers.DeleteArticle()) //

	v1.Get("/users/profile", controllers.GetLoggedInUser())          //
	v1.Get("/users/my-articles", controllers.GetUserArticles())      //
	v1.Patch("/users/edit-profile", controllers.UpdateProfile())     //
	v1.Patch("/users/change-password", controllers.UpdatePassword()) //
	v1.Post("/users/delete-account", controllers.DeleteAccount())

	v1.Use(controllers.Restrict()) //must admin

	//User
	v1.Get("/users", controllers.GetAllUser())
	v1.Get("/users/:id", controllers.GetUser())
	v1.Patch("/users/:id", controllers.UpdateUser())
	v1.Delete("/users/:id", controllers.DeleteUser())

	app.Listen(3000)
}
