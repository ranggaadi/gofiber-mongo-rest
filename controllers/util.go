package controllers

import (
	"errors"
	"fmt"
)

func filterMap(m map[string]interface{}, filters ...string) map[string]interface{} {
	filtered := map[string]interface{}{}
	for k, v := range m {
		for _, val := range filters {
			if val == k {
				continue
			}
			filtered[k] = v
		}
	}
	return filtered
}

func filterMapError(m map[string]interface{}, filters ...string) error {
	for k, _ := range m {
		for _, val := range filters {
			if val == k {
				return errors.New(fmt.Sprintf("You dont have cannot update %s field in this route", k))
			}
		}
	}
	return nil
}

func distinctArray(arr []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range arr {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}
