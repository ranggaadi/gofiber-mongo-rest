package models

type LoginData struct {
	Credential string `json:"credential,omitempty" valid:"required~requiring username or email for login"`
	Password   string `json:"password,omitempty" valid:"required~please provide password,minstringlength(1)~required password for login"`
}

type ResetPasswordRequest struct {
	Password        string `bson:"password" json:"password,omitempty" valid:"required,minstringlength(8)"`
	ConfirmPassword string `bson:"confirmPassword" json:"confirmPassword,omitempty" valid:"sameWithPassword,required,minstringlength(8)"`
}

type ChangePassword struct {
	OldPassword        string `json:"oldPassword,omitempty" valid:"required,minstringlength(1)~please provide old password information"`
	NewPassword        string `json:"newPassword,omitempty" valid:"required,minstringlength(8)~new password at least must contains 8 characters"`
	ConfirmNewPassword string `json:"confirmNewPassword,omitempty" valid:"sameWithPassword~confirm password field different with new password,required"`
}

type DeleteAccount struct {
	Email    string `json:"email,omitempty" valid:"required~please provide email,email~please provide valid email"`
	Password string `json:"password,omitempty" valid:"required~please provide password,minstringlength(1)~required a password for verification"`
}

var EmailMap = map[string]interface{}{
	"email": "email,required",
}
