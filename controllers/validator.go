package controllers

import (
	"github.com/asaskevich/govalidator"
	"github.com/ranggaadi/golang-mongo-api/models"
	"go.mongodb.org/mongo-driver/bson"
)

func validate(model interface{}) (bool, error) {
	result, err := govalidator.ValidateStruct(model)
	if err != nil {
		return false, err
	}
	return result, nil
}

func validateMap(inputMap, mapTemplate bson.M) (bool, error) {
	result, err := govalidator.ValidateMap(inputMap, mapTemplate)
	if err != nil {
		return false, err
	}
	return result, nil
}

func init() {
	govalidator.CustomTypeTagMap.Set("sameWithPassword", func(i interface{}, context interface{}) bool {
		switch v := context.(type) {
		case models.User:
			return v.Password == v.ConfirmPassword
		case models.ResetPasswordRequest:
			return v.Password == v.ConfirmPassword
		case models.ChangePassword:
			return v.NewPassword == v.ConfirmNewPassword
		}
		return false
	})

	// govalidator.CustomTypeTagMap.Set("emailOrUsername", func(i interface{}, context interface{}) bool {
	// 	switch v := context.(type) {
	// 	case models.LoginData:
	// 		// return false
	// 		if v.Username == "" && v.Email != "" {
	// 			return govalidator.IsEmail(v.Email)
	// 		}
	// 		return v.Email == "" && v.Username != ""
	// 		// return (v.Password == "" && v.Email != "") || (v.Email == "" && v.Username != "")
	// 	case map[string]string:
	// 		return v["password"] == v["confirmPassword"]
	// 	}
	// 	return false
	// })
}
