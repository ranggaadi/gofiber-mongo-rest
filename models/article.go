package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/gosimple/slug"
	"github.com/ranggaadi/golang-mongo-api/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

var articles = db.Connect().Collection("articles")

// mengeset kolom nama menjadi unique
var _, _ = articles.Indexes().CreateOne(
	db.Ctx,
	mongo.IndexModel{
		Keys:    bsonx.Doc{{Key: "slug", Value: bsonx.Int32(1)}},
		Options: options.Index().SetUnique(true),
	},
)

type Article struct {
	ID         primitive.ObjectID  `bson:"_id,omitempty" json:"_id,omitempty"`
	Title      string              `bson:"title" json:"title,omitempty" valid:"required,stringlength(3|255)"`
	Slug       string              `bson:"slug" json:"slug,omitempty" valid:"required,minstringlength(1)"`
	Body       string              `bson:"body" json:"body,omitempty" valid:"required,minstringlength(1)"`
	CoverImage string              `bson:"coverImage" json:"coverImage,omitempty" valid:"required,url~please provide coverImage with valid file path"`
	Tags       *[]string           `bson:"tags" json:"tags,omitempty" valid:"required"`
	Author     *primitive.ObjectID `bson:"author,omitempty" json:"author,omitempty" valid:"required"`
	CreatedAt  *time.Time          `bson:"createdAt" json:"createdAt,omitempty" valid:"required"`
	UpdatedAt  *time.Time          `bson:"updatedAt" json:"updatedAt,omitempty" valid:"required"`
}

var ArticleMap = map[string]interface{}{
	"title":      "stringlength(3|255)",
	"body":       "minstringlength(1)",
	"slug":       "minstringlength(1)",
	"tags":       "optional",
	"coverImage": "url~please provide coverImage with valid file path",
	"createdAt":  "notnull,type(time.Time)",
	"updatedAt":  "required,type(time.Time)",
}

func (a *Article) AssignDefault() {
	now := time.Now()
	a.CreatedAt = &now
	a.UpdatedAt = &now

	a.Slug = fmt.Sprintf("%s-%d", slug.Make(a.Title), time.Now().Unix())

	if a.CoverImage == "" {
		a.CoverImage = "article-default-cover.jpg"
	}
}

func (a *Article) SaveArticle() (*mongo.InsertOneResult, error) {

	insertRes, err := articles.InsertOne(db.Ctx, a)
	if err != nil {
		return nil, err
	}

	return insertRes, nil
}

func (a *Article) FindArticleById(slug string) (primitive.M, error) {

	matchStage := bson.D{{"$match", bson.D{{"slug", slug}}}}
	populate := bson.D{
		{"$lookup", bson.D{
			{"from", "users"},
			{"localField", "author"},
			{"foreignField", "_id"},
			{"as", "author"},
		}},
	}

	csr, err := articles.Aggregate(db.Ctx, mongo.Pipeline{matchStage, populate})
	if err != nil {
		return nil, err
	}

	var info []bson.M
	if err = csr.All(db.Ctx, &info); err != nil {
		return nil, err
	}

	if len(info) == 0 {
		return nil, errors.New("There is no article with following id")
	}

	data := info[0]
	currentAuthor := data["author"].(primitive.A)[0].(primitive.M)

	result, _ := json.Marshal(currentAuthor)
	var current User
	err = json.Unmarshal(result, &current)
	if err != nil {
		return nil, err
	}

	current.FilterPopulate()
	data["author"] = current

	return data, nil
}

func (a *Article) GetArticleById(slug string) (*Article, error) {
	val := &Article{}
	res := articles.FindOne(db.Ctx, bson.M{"slug": slug})
	if res.Err() != nil {
		return nil, res.Err()
	}
	res.Decode(val)
	return val, nil
}

// 	// matchStage := bson.D{{"$match", bson.D{{"_id", id}}}}
// 	// populate := bson.D{
// 	// 	{"$lookup", bson.D{
// 	// 		{"from", "users"},
// 	// 		{"localField", "author"},
// 	// 		{"foreignField", "_id"},
// 	// 		{"as", "author"},
// 	// 	}},
// 	// }

// 	// csr, err := articles.Aggregate(db.Ctx, mongo.Pipeline{matchStage, populate})
// 	// if err != nil {
// 	// 	return nil, err
// 	// }

// 	// var info []bson.M
// 	// if err = csr.All(ctx, info); err != nil {
// 	// 	return nil, err
// 	// }

// 	// return info, nil
// }

func (a *Article) SeekArticle(opt bson.M) (*mongo.Cursor, error) {
	val, err := articles.Find(db.Ctx, opt)
	if err != nil {
		return nil, err
	}

	return val, nil
}

func (a *Article) FindArticle(opt bson.M) ([]primitive.M, error) {
	var pipeline mongo.Pipeline

	match := bson.D{
		{"$match", opt},
	}
	populate := bson.D{
		{"$lookup", bson.D{
			{"from", "users"},
			{"localField", "author"},
			{"foreignField", "_id"},
			{"as", "author"},
		}},
	}

	pipeline = mongo.Pipeline{match, populate}

	if len(opt) == 0 {
		pipeline = mongo.Pipeline{populate}
	}

	csr, err := articles.Aggregate(db.Ctx, pipeline)
	if err != nil {
		return nil, err
	}

	var info []bson.M
	if err = csr.All(db.Ctx, &info); err != nil {
		return nil, err
	}

	return info, nil
}

func (a *Article) DeleteArticleById(slug string) (*mongo.DeleteResult, error) {
	val, err := articles.DeleteOne(db.Ctx, bson.M{"slug": slug})
	if err != nil {
		return nil, err
	}

	return val, nil
}

func (a *Article) UpdateArticleById(slug string, updateData bson.M) *mongo.SingleResult {

	return articles.FindOneAndUpdate(db.Ctx,
		bson.M{"slug": slug},
		bson.M{"$set": updateData},
		options.FindOneAndUpdate().SetReturnDocument(1))
}
