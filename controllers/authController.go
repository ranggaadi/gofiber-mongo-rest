package controllers

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/ranggaadi/golang-mongo-api/utils"

	"github.com/asaskevich/govalidator"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber"
	"github.com/ranggaadi/golang-mongo-api/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func signToken(id string) (string, error) {
	expDuration, _ := strconv.Atoi(os.Getenv("JWT_EXPIRE_DURATION"))

	jwtSignatureKey := []byte(os.Getenv("JWT_SIGNATURE_KEY"))
	jwtExpireTime := time.Duration(expDuration) * time.Hour

	var claim models.Claims
	claim.ExpiresAt = time.Now().Add(jwtExpireTime).Unix()
	claim.UserId = id
	claim.IssuedAt = time.Now().Unix()

	signedToken, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claim).SignedString(jwtSignatureKey)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func sendToken(c *fiber.Ctx, usr *models.User) {
	ckDuration, _ := strconv.Atoi(os.Getenv("JWT_COOKIE_DURATION"))
	jwtCookieExpire := time.Duration(ckDuration) * time.Hour

	token, err := signToken(usr.ID.Hex())
	if err != nil || token == "" {
		c.Status(200).JSON(fiber.Map{
			"status":  "fail",
			"message": err.Error(),
		})
		return
	}

	c.Cookie(&fiber.Cookie{
		Name:     "jwt",
		Value:    token,
		HTTPOnly: true,
		Expires:  time.Now().Add(jwtCookieExpire),
		Secure:   false,
	})

	usr.FilterSensitive()

	c.Status(200).JSON(fiber.Map{
		"status": "sucess",
		"token":  token,
		"data":   usr,
	})
}

func Protect() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		var bearerToken string
		cookieJwt := c.Cookies("jwt")

		tokenHeader := c.Get("Authorization")
		if tokenHeader != "" && strings.HasPrefix(tokenHeader, "Bearer") {
			bearerToken = strings.Split(tokenHeader, " ")[1]
		} else if cookieJwt != "" {
			// log.Println(cookieJwt)
			bearerToken = cookieJwt
		}

		if bearerToken == "" {
			c.Status(401).JSON(fiber.Map{
				"status":  "fail",
				"message": "You're not yet logged in!, Please login first",
			})
			return
		}

		token, err := jwt.Parse(bearerToken, func(token *jwt.Token) (interface{}, error) {
			if jwt.GetSigningMethod("HS256") != token.Method {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			// if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			// 	return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			// }

			return []byte(os.Getenv("JWT_SIGNATURE_KEY")), nil
		})

		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		// log.Println("Sukses Parsing token")

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": "Failed to claims token",
			})
			return
		}

		// log.Println("Sukses Claim token")
		// log.Println(claims)

		currentUserId, err := primitive.ObjectIDFromHex(claims["UserId"].(string))
		if !ok || !token.Valid {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		currentUser := user.FindUserById(bson.M{"_id": currentUserId, "active": true})

		if currentUser.ID == primitive.NilObjectID {
			c.Status(401).JSON(fiber.Map{
				"status":  "fail",
				"message": "The user belonging to this token no longer exist",
			})
			return
		}

		if currentUser.ChangedPasswordAfter(claims["iat"].(float64)) {
			c.Status(401).JSON(fiber.Map{
				"status":  "fail",
				"message": "User recently changed password, please login again",
			})
			return
		}

		c.Locals("user", currentUser)
		c.Next()
	}
}

func Restrict() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		loggedUser := c.Locals("user").(*models.User)

		if loggedUser.Role != "admin" {
			c.Status(403).JSON(fiber.Map{
				"status":  "fail",
				"message": "You dont have permission to perform this action",
			})
			return
		}
		c.Next()
	}
}

func Login() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		inputData := &models.LoginData{}
		if err := c.BodyParser(inputData); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Please provide valid data",
			})
			return
		}

		_, err := validate(inputData)
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		var filter bson.M
		if inputData.Credential != "" {
			if govalidator.IsEmail(inputData.Credential) {
				filter = bson.M{"email": inputData.Credential, "active": true}
			} else {
				filter = bson.M{"username": inputData.Credential, "active": true}
			}
		}

		currentUser, err := user.FindFirst(filter, "password")
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": "Incorrect email, username or password",
			})
			return
		}

		if ok := currentUser.CorrectPassword(inputData.Password); !ok {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Incorrect email, username or password",
			})
			return
		}

		currentUser.Password = "" //menghilangkan data sensitif

		sendToken(c, currentUser)
	}
}

func SignUp() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		usr := &models.User{}
		if err := c.BodyParser(usr); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Please provide valid data",
			})
			return
		}

		usr.AssignDefault()
		res, err := validate(usr)
		if err != nil || !res {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		if err := usr.HashPassword(); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		inserted, err := usr.SaveUser()
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		data := usr.FindUserById(bson.M{"_id": inserted.InsertedID.(primitive.ObjectID), "active": true})
		sendToken(c, data)
	}
}

func Logout() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		// c.ClearCookie("jwt") tidak bisa menggunakan ini karena httpOnly
		//maka harus dioveride

		c.Cookie(&fiber.Cookie{
			Name:     "jwt",
			Value:    "8ttp0nly",
			HTTPOnly: true,
			Expires:  time.Now().Add(time.Duration(-10) * time.Second),
			Secure:   false,
		})

		c.Status(200).JSON(fiber.Map{
			"status": "success",
		})
	}
}

func IsAuthorized() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		loggedUser := c.Locals("user").(*models.User)

		currentArticle, err := article.GetArticleById(c.Params("slug"))
		if err != nil {
			c.Status(404).JSON(fiber.Map{
				"status":  "fail",
				"message": "There is no article with that ID",
			})
			return
		}

		if currentArticle.Author.Hex() != loggedUser.ID.Hex() && loggedUser.Role != "admin" {
			c.Status(401).JSON(fiber.Map{
				"status":  "fail",
				"message": "Unauthorized to update or delete this article",
			})
			return
		}

		c.Next()
	}
}

func ForgotPassword() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		var fpReq bson.M
		json.Unmarshal([]byte(c.Body()), &fpReq)

		res, err := validateMap(fpReq, models.EmailMap)
		if err != nil || !res {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		fpReq["active"] = true
		currentUser, err := user.FindFirst(fpReq)
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": "There is no user registered using this email address",
			})
			return
		}

		resetToken, err := currentUser.GenerateResetToken()
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		resetURL := fmt.Sprintf("%s://%s/api/v1/users/reset-password/%s", c.Protocol(), c.Hostname(), resetToken)

		//kirim email
		err = utils.NewEmail(currentUser, resetURL).SendResetPassword()
		if err != nil {
			_ = user.UpdateUserById(bson.M{"_id": currentUser.ID, "active": true}, bson.M{
				"passwordResetToken":  "",
				"passwordResetExpire": nil,
			})

			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": "There was an error while sending an email, Please try again later",
			})
			return
		}

		c.Status(200).JSON(fiber.Map{
			"status":  "success",
			"message": "Email for resetting password successfully sent",
		})
	}
}

func ResetPassword() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		hashedToken := user.CreateSHA256([]byte(c.Params("token")))
		currentUser, err := user.FindFirst(bson.M{
			"passwordResetToken": fmt.Sprintf("%x", hashedToken),
			"passwordResetExpire": bson.M{
				"$gt": time.Now(),
			},
			"active": true,
		})

		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":    "fail",
				"reset url": "Token is invalid or already expired. or user no longer exist",
			})
			return
		}

		req := &models.ResetPasswordRequest{}
		if err := c.BodyParser(req); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Please provide valid data",
			})
			return
		}

		_, err = validate(req)
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		nowMinOneSec := time.Now().Add(time.Duration(-1) * time.Second)
		//dikurangi sedetik karena biasanya pengesavean pada db lebih lama daripada jwt digenerate
		//dan apabila terjadi (passChangedAt > jwtIssuedTimestramp) maka user tidak bisa login, sehingga untuk menjamin, waktu
		//dikurangi sedetik

		currentUser.Password = req.Password

		if err := currentUser.HashPassword(); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		updateData := bson.M{
			"password":            currentUser.Password,
			"passwordResetToken":  "",
			"passwordResetExpire": nil,
			"passwordChangedAt":   &nowMinOneSec,
			"updatedAt":           &nowMinOneSec,
		}

		res := user.UpdateUserById(bson.M{"_id": currentUser.ID, "active": true}, updateData)
		if res.Err() != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": res.Err().Error(),
			})
			return
		}

		returnedData := &models.User{}
		if err = res.Decode(returnedData); err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": res.Err().Error(),
			})
			return
		}

		sendToken(c, returnedData)
	}
}

func UpdatePassword() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		loggedUser := c.Locals("user").(*models.User)

		inputData := &models.ChangePassword{}
		if err := c.BodyParser(inputData); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Please provide valid data",
			})
			return
		}

		_, err := validate(inputData)
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		if !loggedUser.CorrectPassword(inputData.OldPassword) {
			c.Status(401).JSON(fiber.Map{
				"status":  "fail",
				"message": "The current password didn't match",
			})
			return
		}

		nowMinOneSec := time.Now().Add(time.Duration(-1) * time.Second)
		loggedUser.Password = inputData.NewPassword

		if err := loggedUser.HashPassword(); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		updateData := bson.M{
			"password":          loggedUser.Password,
			"passwordChangedAt": &nowMinOneSec,
			"updatedAt":         &nowMinOneSec,
		}

		updated := user.UpdateUserById(bson.M{"_id": loggedUser.ID, "active": true}, updateData)
		if updated.Err() != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": updated.Err().Error(),
			})
			return
		}

		returnedData := &models.User{}
		if err = updated.Decode(returnedData); err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": updated.Err().Error(),
			})
			return
		}

		sendToken(c, returnedData)
	}
}
