package controllers

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/ranggaadi/golang-mongo-api/db"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gofiber/fiber"
	"github.com/ranggaadi/golang-mongo-api/models"
)

var user *models.User

func GetUser() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		oid, err := primitive.ObjectIDFromHex(c.Params("id"))
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Please provide the valid id",
			})
			return
		}

		res := user.FindUserById(bson.M{"_id": oid})

		if res.ID == primitive.NilObjectID {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": fmt.Sprintf("There is no user with id: %s", c.Params("id")),
			})
			return
		}

		c.JSON(fiber.Map{
			"status": "success",
			"data":   res,
		})
	}
}

func GetLoggedInUser() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		loggedUser := c.Locals("user").(*models.User)

		res := user.FindUserById(bson.M{"_id": loggedUser.ID, "active": true})
		if res.ID == primitive.NilObjectID {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "No user found",
			})
			return
		}

		res.FilterSensitive()

		c.JSON(fiber.Map{
			"status": "success",
			"data":   res,
		})
	}
}

func UpdateProfile() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		loggedUser := c.Locals("user").(*models.User)

		var updateData bson.M
		json.Unmarshal([]byte(c.Body()), &updateData)

		// log.Println(updateData)

		if updateData["updatedAt"] == nil || updateData["updatedAt"].(time.Time) != time.Now() {
			updateData["updatedAt"] = time.Now()
		}

		res, err := validateMap(updateData, models.UserMap)
		if err != nil || !res {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		filter := []string{
			"_id",
			"password",
			"role",
			"active",
			"confirmPassword",
			"passwordChangedAt",
			"passwordResetToken",
			"passwordResetExpire",
			"createdAt",
		}

		if err = filterMapError(updateData, filter...); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		result := user.UpdateUserById(bson.M{"_id": loggedUser.ID, "active": true}, updateData)
		if result.Err() != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": result.Err().Error(),
			})
			return
		}

		returnedData := &models.User{}
		if err = result.Decode(returnedData); err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": result.Err().Error(),
			})
			return
		}

		returnedData.FilterSensitive()

		c.Status(200).JSON(fiber.Map{
			"status": "success",
			"data":   returnedData,
		})
	}
}

func GetAllUser() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		res, err := user.FindUser(bson.M{})
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		resAll := []models.User{}
		if err = res.All(db.Ctx, &resAll); err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		c.Status(200).JSON(fiber.Map{
			"status": "success",
			"data":   resAll,
		})
	}
}

func UpdateUser() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		oid, err := primitive.ObjectIDFromHex(c.Params("id"))
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Please provide the valid id",
			})
			return
		}

		var updateData bson.M
		json.Unmarshal([]byte(c.Body()), &updateData)

		// log.Println(updateData)

		if updateData["updatedAt"] == nil || updateData["updatedAt"].(time.Time) != time.Now() {
			updateData["updatedAt"] = time.Now()
		}

		res, err := validateMap(updateData, models.UserMap)
		if err != nil || !res {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		filter := []string{
			"_id",
			"passwordChangedAt",
			"passwordResetExpire",
			"createdAt",
		}

		if err = filterMapError(updateData, filter...); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		result := user.UpdateUserById(bson.M{"_id": oid}, updateData)
		if result.Err() != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": result.Err().Error(),
			})
			return
		}

		returnedData := &models.User{}
		if err = result.Decode(returnedData); err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": result.Err().Error(),
			})
			return
		}

		c.Status(200).JSON(fiber.Map{
			"status": "success",
			"data":   returnedData,
		})
	}
}

func DeleteUser() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		oid, err := primitive.ObjectIDFromHex(c.Params("id"))
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Please provide the valid id",
			})
			return
		}

		res := user.FindUserById(bson.M{"_id": oid})
		if res.ID == primitive.NilObjectID {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "No user found",
			})
			return
		}

		deleted, err := user.DeleteUserById(oid)
		if deleted.DeletedCount > 0 {
			c.Status(200).JSON(fiber.Map{
				"status": "success",
				"data":   res,
			})
			return
		}

		c.Status(400).JSON(fiber.Map{
			"status": "fail",
			"data":   "There is no item to be deleted",
		})
	}
}

func DeleteAccount() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		loggedUser := c.Locals("user").(*models.User)

		inputData := &models.DeleteAccount{}
		if err := c.BodyParser(inputData); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Please provide valid data",
			})
			return
		}

		_, err := validate(inputData)
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		if loggedUser.Email != inputData.Email || !loggedUser.CorrectPassword(inputData.Password) {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Incorrect email or password for validation",
			})
			return
		}

		res := user.UpdateUserById(bson.M{"_id": loggedUser.ID, "active": true}, bson.M{"active": false})
		if res.Err() != nil {
			c.Status(500).JSON(fiber.Map{
				"status": "fail",
				"data":   err.Error(),
			})
			return
		}

		c.Cookie(&fiber.Cookie{
			Name:     "jwt",
			Value:    "8ttp0nly",
			HTTPOnly: true,
			Expires:  time.Now().Add(time.Duration(-10) * time.Second),
			Secure:   false,
		})

		c.Status(200).JSON(fiber.Map{
			"status": "success",
			"data":   "Your account successfully deleted",
		})
	}
}
