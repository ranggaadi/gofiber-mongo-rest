package utils

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/matcornic/hermes/v2"
	"github.com/ranggaadi/golang-mongo-api/models"
	"gopkg.in/gomail.v2"
	"jaytaylor.com/html2text"
)

type Email struct {
	Name string
	Url  string
	From string
	To   string
}

func NewEmail(user *models.User, url string) *Email {
	return &Email{user.Name, url, "Animek Corp. <admin@animektothemek.com>", user.Email}
}

func (e *Email) GenerateTemplate(purpose string) string {
	var email hermes.Email

	h := hermes.Hermes{
		Product: hermes.Product{
			Name: "Animek",
			Link: "https://animektothemek.com/",
			// Optional product logo
			Logo:      "https://gamepedia.cursecdn.com/bindingofisaacre_gamepedia/0/0f/Boss_Delirium.png",
			Copyright: "Copyright © 2020 Animek. All rights reserved.",
		},
	}

	if purpose == "welcome" {
		email = hermes.Email{
			Body: hermes.Body{
				Name: e.Name,
				Intros: []string{
					"Welcome to Animek, we glad to have you!\nWe're all a big familiy here, so make sure to upload your user photo so we get to know you a bit better!",
				},
				Actions: []hermes.Action{
					{
						Instructions: "To go customizing your profile, please click here:",
						Button: hermes.Button{
							Color: "#22BC66", // Optional action button color
							Text:  "Upload user photo",
							Link:  e.Url,
						},
					},
				},
				Outros: []string{
					"Need help, or have questions? Just reply to this email, we'd love to help.\n - Bakekok Bunda, CEO",
				},
			},
		}
	} else if purpose == "resetPassword" {
		email = hermes.Email{
			Body: hermes.Body{
				Name: e.Name,
				Intros: []string{
					"You have received this email because a password reset request for Animek account was received.\nIf you never send request about forgot password. just ignore this message",
				},
				Actions: []hermes.Action{
					{
						Instructions: "Click the button below to reset your password:",
						Button: hermes.Button{
							Color: "#DC4D2F",
							Text:  "Reset your password",
							Link:  e.Url,
						},
					},
				},
				Outros: []string{
					fmt.Sprintf("If you did not request a password reset, please ignore this email\n if theres a problem with the button just send PATCH request with body of \"password\" and \"confirmPassword\" to this url: %s ", e.Url),
				},
				Signature: "Thanks\n - Bakekok Bunda, CEO",
			},
		}
	}

	emailBody, err := h.GenerateHTML(email)
	if err != nil {
		return ""
	}

	return emailBody
}

func (e *Email) send(purpose, subject string) error {
	emailStr := e.GenerateTemplate(purpose)
	if emailStr == "" {
		return errors.New("There's an error while generating email template.")
	}

	text, err := html2text.FromString(emailStr, html2text.Options{PrettyTables: true})
	if err != nil {
		return err
	}

	header := map[string][]string{
		"From":    {e.From},
		"To":      {e.To},
		"Subject": {subject},
	}

	mailer := gomail.NewMessage()
	mailer.SetHeaders(header)
	mailer.SetBody("text/plain", text)
	mailer.AddAlternative("text/html", emailStr)

	err = e.NewDialer().DialAndSend(mailer)
	if err != nil {
		return err
	}

	return nil
}

func (e *Email) SendResetPassword() error {
	err := e.send("resetPassword", "Animek reset password (valid for 1 hour)")
	if err != nil {
		return err
	}

	return nil
}

func (e *Email) SendWelcome() error {
	err := e.send("welcome", "Welcome to Animek!")

	if err != nil {
		return err
	}

	return nil
}

func (e *Email) NewDialer() *gomail.Dialer {
	emailPort, _ := strconv.Atoi(os.Getenv("EMAIL_PORT"))
	return gomail.NewDialer(
		os.Getenv("EMAIL_HOST"),
		emailPort,
		os.Getenv("EMAIL_USERNAME"),
		os.Getenv("EMAIL_PASSWORD"),
	)
}
