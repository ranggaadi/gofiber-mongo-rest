package models

import (
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"time"

	"github.com/ranggaadi/golang-mongo-api/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"golang.org/x/crypto/bcrypt"
)

var users = db.Connect().Collection("users")

// mengeset kolom nama menjadi unique
var _, _ = users.Indexes().CreateMany(
	db.Ctx,
	[]mongo.IndexModel{
		{
			Keys:    bsonx.Doc{{Key: "username", Value: bsonx.Int32(1)}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bsonx.Doc{{Key: "email", Value: bsonx.Int32(1)}},
			Options: options.Index().SetUnique(true),
		},
	},
)

type User struct {
	ID                  primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty" valid:"-"`
	Name                string             `bson:"name" json:"name,omitempty" valid:"required,stringlength(3|26)"`
	Username            string             `bson:"username" json:"username,omitempty" valid:"required,stringlength(3|26),matches(^[a-z.0-9]*$)"`
	Biography           string             `bson:"biography" json:"biography,omitempty" valid:"optional,stringlength(0|255)"`
	UserImage           string             `bson:"userImage" json:"userImage,omitempty" valid:"required,url~please provide userImage with valid file path"`
	Email               string             `bson:"email" json:"email,omitempty" valid:"required,email"`
	Role                string             `bson:"role" json:"role,omitempty" valid:"required,in(user|admin)"`
	Password            string             `bson:"password" json:"password,omitempty" valid:"required,minstringlength(8)"`
	ConfirmPassword     string             `bson:"confirmPassword" json:"confirmPassword,omitempty" valid:"sameWithPassword,required,minstringlength(8)"`
	PasswordChangedAt   *time.Time         `bson:"passwordChangedAt" json:"passwordChangedAt,omitempty" valid:"optional"`
	PasswordResetToken  string             `bson:"passwordResetToken" json:"passwordResetToken,omitempty" valid:"optional"`
	PasswordResetExpire *time.Time         `bson:"passwordResetExpire" json:"passwordResetExpire,omitempty" valid:"optional"`
	Active              *bool              `bson:"active" json:"active,omitempty" valid:"required"`
	CreatedAt           *time.Time         `bson:"createdAt" json:"createdAt,omitempty" valid:"required"`
	UpdatedAt           *time.Time         `bson:"updatedAt" json:"updatedAt,omitempty" valid:"required"`
}

var UserMap = map[string]interface{}{
	"name":                "stringlength(3|26)",
	"username":            "stringlength(3|26),matches(^[a-z.0-9]*$)",
	"biography":           "optional,stringlength(0|255)",
	"userImage":           "url~please provide userImage with valid file path",
	"email":               "email",
	"role":                "in(user|admin)",
	"password":            "minstringlength(8)",
	"confirmPassword":     "minstringlength(8)",
	"passwordChangedAt":   "notnull,type(time.Time)",
	"passwordResetToken":  "notnull",
	"passwordResetExpire": "notnull,type(time.Time)",
	"active":              "notnull,type(bool)",
	"createdAt":           "notnull,type(time.Time)",
	"updatedAt":           "required,type(time.Time)",
}

func (u *User) AssignDefault() {
	now := time.Now()
	u.CreatedAt = &now
	u.UpdatedAt = &now

	if u.UserImage == "" {
		u.UserImage = "user-default.jpg"
	}

	if u.Role == "" {
		u.Role = "user"
	}

	cond := true
	u.Active = &cond
}

func (u *User) FilterSensitive() {
	u.Role = ""
	u.Password = ""
	u.ConfirmPassword = ""
	u.PasswordChangedAt = nil
	u.PasswordResetToken = ""
	u.PasswordResetExpire = nil
	u.Active = nil
}

func (u *User) FilterPopulate() {
	u.FilterSensitive()
	u.Email = ""
	u.Username = ""
	u.CreatedAt = nil
	u.UpdatedAt = nil
	return
}

func (u *User) HashPassword() error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), 12)
	if err != nil {
		return err
	}
	u.Password = string(bytes)
	u.ConfirmPassword = ""
	return nil
}

func (u *User) SaveUser() (*mongo.InsertOneResult, error) {
	insertRes, err := users.InsertOne(db.Ctx, u)
	if err != nil {
		return nil, err
	}

	return insertRes, nil
}

func (u *User) FindUserById(opt bson.M, exclude ...string) *User {
	val := &User{}
	var projection bson.M

	if len(exclude) != 0 {
		projection = bson.M{
			"role":                0,
			"active":              0,
			"password":            0,
			"confirmPassword":     0,
			"passwordChangedAt":   0,
			"passwordResetToken":  0,
			"passwordResetExpire": 0,
		}

		for _, v := range exclude {
			delete(projection, v)
		}
	}

	// log.Println(projection)

	res := users.FindOne(db.Ctx, opt, options.FindOne().SetProjection(projection))
	res.Decode(val)
	return val
}

func (u *User) FindUser(opt bson.M, exclude ...string) (*mongo.Cursor, error) {
	var projection bson.M

	if len(exclude) != 0 {
		projection = bson.M{
			"role":                0,
			"active":              0,
			"password":            0,
			"confirmPassword":     0,
			"passwordChangedAt":   0,
			"passwordResetToken":  0,
			"passwordResetExpire": 0,
		}

		for _, v := range exclude {
			delete(projection, v)
		}
	}

	// log.Println(projection)

	val, err := users.Find(db.Ctx, opt, options.Find().SetProjection(projection))
	if err != nil {
		return nil, err
	}

	return val, nil
}

func (u *User) FindFirst(opt bson.M, exclude ...string) (*User, error) {
	val := &User{}
	var projection bson.M

	if len(exclude) != 0 {
		projection = bson.M{
			"role":                0,
			"active":              0,
			"password":            0,
			"confirmPassword":     0,
			"passwordChangedAt":   0,
			"passwordResetToken":  0,
			"passwordResetExpire": 0,
		}

		for _, v := range exclude {
			delete(projection, v)
		}
	}

	// log.Println(projection)

	res := users.FindOne(db.Ctx, opt, options.FindOne().SetProjection(projection))
	if res.Err() != nil {
		return nil, res.Err()
	}

	res.Decode(val)
	return val, nil
}

func (u *User) DeleteUserById(id primitive.ObjectID) (*mongo.DeleteResult, error) {
	val, err := users.DeleteOne(db.Ctx, bson.M{"_id": id})
	if err != nil {
		return nil, err
	}

	return val, nil
}

func (User *User) UpdateUserById(opt, updateData bson.M) *mongo.SingleResult {
	return users.FindOneAndUpdate(db.Ctx,
		opt,
		bson.M{"$set": updateData},
		options.FindOneAndUpdate().SetReturnDocument(1))
}

func (u *User) GenerateResetToken() (string, error) {
	key := make([]byte, 32)
	if _, err := rand.Read(key); err != nil {
		return "", err
	}

	now := time.Now()
	nowPlusAnHour := now.Add(time.Duration(1) * time.Hour) //valid selama 1 jam
	bit16 := fmt.Sprintf("%x", key)
	sha256 := u.CreateSHA256([]byte(bit16))

	res := users.FindOneAndUpdate(db.Ctx,
		bson.M{"_id": u.ID},
		bson.M{"$set": bson.M{
			"passwordResetToken":  fmt.Sprintf("%x", sha256),
			"passwordResetExpire": &nowPlusAnHour,
			"updatedAt":           &now,
		}})

	if res.Err() != nil {
		return "", res.Err()
	}

	return bit16, nil
}

func (u *User) CreateSHA256(data []byte) []byte {
	sha := sha256.New()
	sha.Write(data)

	return sha.Sum(nil)
}

func (u *User) CorrectPassword(password string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password)); err != nil {
		return false
	}

	return true
}

func (u *User) ChangedPasswordAfter(jwtIssuedAt float64) bool {
	if u.PasswordChangedAt != nil {
		return float64(u.PasswordChangedAt.Unix()) > jwtIssuedAt
	}

	return false
}
