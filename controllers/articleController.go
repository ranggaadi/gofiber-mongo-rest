package controllers

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/gosimple/slug"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gofiber/fiber"
	"github.com/ranggaadi/golang-mongo-api/db"
	"github.com/ranggaadi/golang-mongo-api/models"
)

var article *models.Article

func Landing() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		c.JSON(fiber.Map{
			"status": "Success",
		})
	}
}

func GetArticle() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		res, err := article.FindArticleById(c.Params("slug"))
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		c.JSON(fiber.Map{
			"status": "success",
			"data":   res,
		})
	}
}

func GetAllArticle() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		res, err := article.FindArticle(bson.M{})
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		for _, v := range res {
			data := v["author"].(primitive.A)[0].(primitive.M)

			result, _ := json.Marshal(data)
			var current models.User
			err = json.Unmarshal(result, &current)
			if err != nil {
				c.Status(500).JSON(fiber.Map{
					"status":  "fail",
					"message": err.Error(),
				})
				return
			}

			current.FilterPopulate()
			v["author"] = current
		}

		c.Status(200).JSON(fiber.Map{
			"status": "success",
			"data":   res,
		})
	}
}

func CreateArticle() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		loggedUser := c.Locals("user").(*models.User)

		var tags []string

		var bodyData bson.M
		if err := json.Unmarshal([]byte(c.Body()), &bodyData); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Please provide valid data",
			})
			return
		}

		if bodyData["tags"] != nil && bodyData["tags"].(string) != "" {
			tags = strings.Split(strings.ReplaceAll(bodyData["tags"].(string), " ", ""), ",")
		} else {
			tags = []string{}
		}

		uniqTags := distinctArray(tags)
		bodyData["tags"] = &uniqTags

		data, _ := json.Marshal(bodyData)

		var art models.Article
		_ = json.Unmarshal(data, &art)

		art.Author = &loggedUser.ID
		art.AssignDefault()

		res, err := validate(art)
		if err != nil || !res {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		_, err = art.SaveArticle()
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		result, err := art.FindArticleById(art.Slug)
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		c.Status(200).JSON(fiber.Map{
			"status": "success",
			"data":   result,
		})
	}
}

func UpdateArticle() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		var updateData bson.M
		var tags []string

		if err := json.Unmarshal([]byte(c.Body()), &updateData); err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": "Invalid JSON format",
			})
		}

		if updateData["tags"] != nil && updateData["tags"].(string) != "" {
			tags = strings.Split(strings.ReplaceAll(updateData["tags"].(string), " ", ""), ",")

			uniqTags := distinctArray(tags)
			updateData["tags"] = &uniqTags
		} else if updateData["tags"] != nil && updateData["tags"].(string) == "" {
			tags = []string{}
			updateData["tags"] = &tags
		}

		if updateData["updatedAt"] == nil || updateData["updatedAt"].(time.Time) != time.Now() {
			updateData["updatedAt"] = time.Now()
		}

		if updateData["slug"] != nil && updateData["slug"].(string) != "" { //memastikan slug kosong
			updateData["slug"] = c.Params("slug")
		}

		if updateData["title"] != nil && updateData["title"].(string) != "" {
			updateData["slug"] = fmt.Sprintf("%s-%d", slug.Make(updateData["title"].(string)), time.Now().Unix())
		}

		_, err := validateMap(updateData, models.ArticleMap)
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		result := article.UpdateArticleById(c.Params("slug"), updateData)
		if result.Err() != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": result.Err().Error(),
			})
			return
		}

		returnedData := &models.Article{}
		if err = result.Decode(returnedData); err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": result.Err().Error(),
			})
			return
		}

		res, err := article.FindArticleById(returnedData.Slug)
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		c.Status(200).JSON(fiber.Map{
			"status": "success",
			"data":   res,
		})
	}
}

func DeleteArticle() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {

		res, err := article.FindArticleById(c.Params("slug"))
		if err != nil {
			c.Status(400).JSON(fiber.Map{
				"status": "fail",
				"data":   err.Error(),
			})
		}

		deleted, err := article.DeleteArticleById(c.Params("slug"))
		if deleted.DeletedCount > 0 {
			c.Status(200).JSON(fiber.Map{
				"status": "success",
				"data":   res,
			})
			return
		}

		c.Status(400).JSON(fiber.Map{
			"status": "fail",
			"data":   "There is no item to be deleted",
		})
	}
}

func GetUserArticles() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		loggedUser := c.Locals("user").(*models.User)

		res, err := article.SeekArticle(bson.M{"author": loggedUser.ID})
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail bro",
				"message": err.Error(),
			})
			return
		}

		resAll := []models.Article{}
		if err = res.All(db.Ctx, &resAll); err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail sanagt",
				"message": err.Error(),
			})
			return
		}

		c.Status(200).JSON(fiber.Map{
			"status": "success",
			"data":   resAll,
		})
	}
}

func GetArticleByTag() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		tag := c.Params("tag")

		res, err := article.FindArticle(bson.M{"tags": bson.M{"$elemMatch": bson.M{"$eq": tag}}})
		if err != nil {
			c.Status(500).JSON(fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		for _, v := range res {
			data := v["author"].(primitive.A)[0].(primitive.M)

			result, _ := json.Marshal(data)
			var current models.User
			err = json.Unmarshal(result, &current)
			if err != nil {
				c.Status(500).JSON(fiber.Map{
					"status":  "fail",
					"message": err.Error(),
				})
				return
			}

			current.FilterPopulate()
			v["author"] = current
		}

		c.Status(200).JSON(fiber.Map{
			"status": "success",
			"data":   res,
		})
	}
}
